# Exam 1

Bank Service

## Prerequisites

- Installed [Node.js](https://nodejs.org/).

## Run Tests

Install dependencies, then run `npm test`:

```bash
$ npm install
$ npm test
```
