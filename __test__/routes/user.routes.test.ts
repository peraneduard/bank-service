import request from "supertest";

import app from "../../src/app";

describe("Accounts routes", () => {
  test("Get all accounts", async () => {
    const res = await request(app).get("/accounts");
    expect(Array.isArray(res.body)).toBe(true);
  });
});
