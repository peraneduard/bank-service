import accountService from "../../src/services/account.services";
import ledgerService from "../../src/services/ledger.services";
import { emptyAccountJson, emptyLedgerJson } from "../../src/utils";

const dataTeardown = async () => {
  await emptyAccountJson();
  await emptyLedgerJson();
};

describe("Test Account and Ledger Service", () => {
  let accountId: number;
  let secondAccountId: number;
  let firstAccountInitialDepositAmount: number = 100;
  let secondAccountInitialDepositAmount: number = 100;
  let firstAccountExpectedBalance: number = firstAccountInitialDepositAmount;
  let secondAccountExpectedBalance: number = secondAccountInitialDepositAmount;

  beforeAll(async () => {
    await dataTeardown();

    secondAccountId = await accountService.createAccount({
      name: "John Kerr G. Peran",
      despositAmout: secondAccountInitialDepositAmount,
    });
  });

  describe("Open Bank Account", () => {
    it("should create and return the first new created account ID", async () => {
      accountId = await accountService.createAccount({
        name: "Eduard G. Peran",
        despositAmout: firstAccountInitialDepositAmount,
      });

      expect(accountId).not.toBeNull();
    });
  });

  describe("Make Account Deposit", () => {
    it("should create and return the transaction ID", async () => {
      const amount = 100;
      firstAccountExpectedBalance = firstAccountExpectedBalance + amount;
      const transId = await accountService.deposit({ accountId, amount });

      expect(transId).not.toBeNull();
    });
  });

  describe("Make Money Transfer", () => {
    it("should transfer money to another customer and return the transaction ID", async () => {
      const amountToBeSend = 50;
      firstAccountExpectedBalance =
        firstAccountExpectedBalance - amountToBeSend;
      secondAccountExpectedBalance =
        secondAccountExpectedBalance + amountToBeSend;

      const transId = await accountService.sendMoney({
        accountId,
        amount: amountToBeSend,
        receiverAccountId: secondAccountId,
      });

      expect(transId).not.toBeNull();
    });
  });

  describe("Check Account Balance", () => {
    it("should return the balance amount", async () => {
      const b = await accountService.checkBalance({ accountId });
      expect(b).toEqual(firstAccountExpectedBalance);
    });
  });

  describe("Withdraw amount", () => {
    test("should throw an error when customer attempts to withdraw more money than they have in their account", async () => {
      await expect(() =>
        accountService.withdrawMoney({ accountId, amount: 10000 })
      ).rejects.toThrow("Insufficient amount!");
    });
  });

  describe("Check Bank Total Balance", () => {
    it("should return the total balance amount of the bank", async () => {
      const b = await ledgerService.checkBankBalance();
      expect(b).toEqual(
        firstAccountExpectedBalance + secondAccountExpectedBalance
      );
    });
  });
});
