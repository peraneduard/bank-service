export enum TransactionType {
  SENT = "SENT",
  RECEIVED = "RECEIVED",
  DEPOSIT = "DEPOSIT",
  WITHDRAW = "WITHDRAW",
}
