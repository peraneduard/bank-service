import { TransactionType } from "./enums";

export interface Ledger {
  transactionId: number;
  accountId: number;
  amount: number;
  senderAccountId?: number;
  receiverAccountId?: number;
  remarks: string;
  type: TransactionType;
}
