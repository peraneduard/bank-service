import { Router } from "express";
import {
  getAccountsController,
  getAccountController,
  addAccountController,
  accountDepositController,
  sendMoneyController,
  checkBalanceController,
  withdrawMoneyController,
} from "../controllers/account.controllers";

const router = Router();
router.route("/").get(getAccountsController).post(addAccountController);
router.route("/:accountId").get(getAccountController);
router.route("/balance/:accountId").put(checkBalanceController);
router.route("/deposit/:accountId").put(accountDepositController);
router.route("/send-money/:accountId").put(sendMoneyController);
router.route("/withdraw/:accountId").put(withdrawMoneyController);

export { router };
