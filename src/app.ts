import express, { Application, Request, Response, NextFunction } from "express";
import * as bodyParser from "body-parser";
import { router as accountRoutes } from "./routes/account.routes";

const app: Application = express();

// middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// route
app.use("/api/v1/accounts", accountRoutes);
app.use("/", (req: Request, res: Response, next: NextFunction): void => {
  res.json({ message: "Allo! Catch-all route." });
});

export default app;
