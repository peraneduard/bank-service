import * as fs from "fs";
import { Account } from "../models";

const accountDataPath = "./data/accounts.json";

export const getAccountsJson = async (): Promise<Account[]> => {
  const json = await fs.promises.readFile(accountDataPath);
  const data = JSON.parse(json as any);
  return Object.values(data);
};

export const saveAccountJson = async (data: Account): Promise<void> => {
  let existAccounts = await getAccountsJson();
  existAccounts.push(data);

  const stringifyData = JSON.stringify(
    existAccounts.reduce((d, a) => {
      d = { ...d, [a.id]: a };
      return d;
    }, {})
  );
  await fs.promises.writeFile(accountDataPath, stringifyData);
};

export const emptyAccountJson = async (): Promise<void> => {
  await fs.promises.writeFile(accountDataPath, "{}");
};
