import * as fs from "fs";
import { Ledger } from "../models";

const ledgerDataPath = "./data/ledger.json";

export const getLedgersJson = async (): Promise<Ledger[]> => {
  const json = await fs.promises.readFile(ledgerDataPath);
  const data = JSON.parse(json as any);
  return Object.values(data);
};

export const saveLedgerJson = async (data: Ledger): Promise<void> => {
  let existLedgers = await getLedgersJson();
  existLedgers.push(data);

  const stringifyData = JSON.stringify(
    existLedgers.reduce((d, a) => {
      d = { ...d, [a.transactionId]: a };
      return d;
    }, {})
  );
  await fs.promises.writeFile(ledgerDataPath, stringifyData);
};

export const emptyLedgerJson = async (): Promise<void> => {
  await fs.promises.writeFile(ledgerDataPath, "{}");
};
