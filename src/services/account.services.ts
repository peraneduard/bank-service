import { Account, TransactionType } from "../models";
import { getAccountsJson, saveAccountJson } from "../utils";
import ledgerService from "./ledger.services";

const getAccounts = async (): Promise<Account[]> => {
  return await getAccountsJson();
};

const createAccount = async (params: {
  name: string;
  despositAmout: number;
}): Promise<Account> => {
  const { name, despositAmout } = params;
  const newAccountId = Math.floor(100000 + Math.random() * 900000);

  const a: Account = {
    id: newAccountId,
    name: name,
    initialDeposit: despositAmout,
  };

  await saveAccountJson(a);

  await ledgerService.createLedger({
    accountId: newAccountId,
    amount: despositAmout,
    transType: TransactionType.DEPOSIT,
  });

  return a;
};

const deposit = async (params: {
  accountId: number;
  amount: number;
}): Promise<number> => {
  const { accountId, amount } = params;

  return await ledgerService.createLedger({
    accountId,
    amount,
    transType: TransactionType.DEPOSIT,
  });
};

const sendMoney = async (params: {
  accountId: number;
  amount: number;
  receiverAccountId: number;
}): Promise<number> => {
  const { accountId, amount, receiverAccountId } = params;

  const sentTransId = await ledgerService.createLedger({
    accountId,
    amount,
    senderReceiverAccountId: receiverAccountId,
    transType: TransactionType.SENT,
  });

  await ledgerService.createLedger({
    accountId: receiverAccountId,
    amount,
    senderReceiverAccountId: accountId,
    transType: TransactionType.RECEIVED,
  });

  return sentTransId;
};

const checkBalance = async (params: { accountId: number }): Promise<number> => {
  const { accountId } = params;

  const ledgers = await ledgerService.getLedgers();
  const accountLedegers = ledgers.filter((l) => l.accountId === accountId);

  return accountLedegers.reduce((b, l) => {
    b += l.amount;

    return b;
  }, 0);
};

const withdrawMoney = async (params: {
  accountId: number;
  amount: number;
}): Promise<number> => {
  const { accountId, amount } = params;

  const currentBal = await checkBalance({ accountId });

  if (amount > currentBal) throw new Error("Insufficient amount!");

  return await ledgerService.createLedger({
    accountId,
    amount,
    transType: TransactionType.WITHDRAW,
  });
};

export default {
  getAccounts,
  createAccount,
  deposit,
  sendMoney,
  checkBalance,
  withdrawMoney,
};
