import { Ledger, TransactionType } from "../models";
import { getLedgersJson, saveLedgerJson } from "../utils/ledger";

const getLedgers = async (): Promise<Ledger[]> => {
  return await getLedgersJson();
};

const createLedger = async (params: {
  accountId: number;
  amount: number;
  transType: TransactionType;
  senderReceiverAccountId?: number;
}): Promise<number> => {
  const { accountId, amount, transType, senderReceiverAccountId } = params;
  const newTranId = Math.floor(100000 + Math.random() * 900000);
  let senderAccountId, receiverAccountId;

  if (transType === TransactionType.SENT)
    receiverAccountId = senderReceiverAccountId;

  if (transType === TransactionType.RECEIVED)
    senderAccountId = senderReceiverAccountId;

  const a = [TransactionType.SENT, TransactionType.WITHDRAW].includes(transType)
    ? -Math.abs(amount)
    : amount;

  await saveLedgerJson({
    transactionId: newTranId,
    accountId,
    amount: a,
    type: transType,
    senderAccountId,
    receiverAccountId,
    remarks: "",
  });

  return newTranId;
};

const checkBankBalance = async (): Promise<number> => {
  const ledgers = await getLedgers();
  return ledgers.reduce((b, l) => {
    b += l.amount;

    return b;
  }, 0);
};

export default { getLedgers, createLedger, checkBankBalance };
