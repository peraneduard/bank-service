import { Request, Response } from "express";
import accountService from "../services/account.services";

export const getAccountsController = async (
  req: Request,
  res: Response
): Promise<void> => {
  const accounts = await accountService.getAccounts();
  res.status(200).json(accounts);
};

export const getAccountController = async (
  req: Request,
  res: Response
): Promise<void> => {
  const accounts = await accountService.getAccounts();
  res.status(200).json(accounts.find((a) => a.id === +req.params.accountId));
};

export const addAccountController = async (
  req: Request,
  res: Response
): Promise<void> => {
  const { name, amount } = req.body;
  const account = await accountService.createAccount({
    name,
    despositAmout: amount,
  });
  res.status(200).json(account);
};

export const accountDepositController = async (
  req: Request,
  res: Response
): Promise<void> => {
  const { amount } = req.body;
  const transId = await accountService.deposit({
    accountId: +req.params.accountId,
    amount,
  });
  res.status(200).json({ transactionId: transId });
};

export const sendMoneyController = async (
  req: Request,
  res: Response
): Promise<void> => {
  const { amount, receiverAccountId } = req.body;
  const transId = await accountService.sendMoney({
    accountId: +req.params.accountId,
    amount,
    receiverAccountId,
  });
  res.status(200).json({ transactionId: transId });
};

export const checkBalanceController = async (
  req: Request,
  res: Response
): Promise<void> => {
  const balance = await accountService.checkBalance({
    accountId: +req.params.accountId,
  });
  res.status(200).json(balance);
};

export const withdrawMoneyController = async (
  req: Request,
  res: Response
): Promise<void> => {
  const { amount } = req.body;
  const transId = await accountService.withdrawMoney({
    accountId: +req.params.accountId,
    amount,
  });
  res.status(200).json({ transactionId: transId });
};
